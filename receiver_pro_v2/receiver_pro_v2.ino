#define CE_PIN 9
#define CSN_PIN 10
#define ONE_WIRE_BUS 2
#define DEBUG

#include <RF24.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#include "radio.h"

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

int get_temp()
{
    sensors.requestTemperatures();
    return sensors.getTempCByIndex(0) * 100;
}

void start_temp()
{
    for (int i = 0; i < 10; ++i)
    {
        int temp = get_temp();
        if (temp != 8500)
        {
            _p("Temp sensor initialized, retries: %d", i);
            _p("Measured temp: %d", temp);
            return;
        }
        delay(100);
    }
    _p("Error: temp sensor initialization failed");
}

void start_serial()
{
    setup_serial();
    _p("Serial initialized");
}

/*

  void handle_temp() {
  float temp = get_temp();

  Serial.print("Measured temprerature: ");
  Serial.println(temp);

  payload.cmd = 'T';
  payload.data = (payload.data << 16) + (unsigned long)(temp * 100);
  }

  void handle_light() {
  int brightness = map(payload.data, 0, 100, 0, 255);
  payload.data = (payload.data << 16) + brightness;
  payload.cmd = 'L';

  Serial.print("Set brightness to: ");
  Serial.println(brightness);

  analogWrite(LIGHT, brightness);
  }

*/

void start_radio()
{
    setup_radio();

    radio.openWritingPipe(device[0]);
    radio.openReadingPipe(1, device[1]);

    _p("Radio initialized");
    radio_print();

    message.cmd = Command::REQ_PING;
    while (true)
    {
        if (radio_write())
        {
            break;
        }
        delay(1000);
    }
}

void setup()
{
    start_serial();
    start_radio();
    start_temp();
}

bool handle_ping_request()
{
    make_ping_response();

    return true;
}

bool handle_ping_response()
{
    uint32_t timestamp = micros();
    _p("Ping: %dms", (timestamp - message.data.dword) / 1000);

    return false;
}

bool handle_temp_request()
{
    message.cmd = Command::RET_TEMP;
    message.data.dword = get_temp();

    _p("Measured temp: %d", message.data.dword);

    return true;
}

bool process_message()
{
    _p("Processing command: %hhu", message.cmd);

    if (message.cmd == Command::REQ_PING)
    {
        _p("Handle ping request");
        return handle_ping_request();
    }
    else if (message.cmd == Command::RET_PING)
    {
        _p("Handle ping response");
        return handle_ping_response();
    }
    else if (message.cmd == Command::REQ_TEMP)
    {
        _p("Handle temp request");
        return handle_temp_request();
    }
        /*else if (payload.cmd == 'l')
        {
            handle_light();
        }*/
    else
    {
        _p("Warning: command not supported");
    }

    return false;
}

void loop(void)
{
    if (radio_read())
    {
        if (process_message())
        {
            radio_write();
        }
    }

    delay(50);

    /*delay(3000);
    digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
    message.data.dword = 1;
    radio_write();*/
    /*for (int i = 0; i < 10; ++i) {
      payload.retry = i;
      if (radio.write(&payload, sizeof(payload))) {
        break;
      }
      }*/
    //delay(1000);                       // wait for a second

    //digitalWrite(3, LOW);    // turn the LED off by making the voltage LOW
    //message.data.dword = 0;
    //radio_write();
    /*for (int i = 0; i < 10; ++i) {
      payload.retry = i;
      if (radio.write(&payload, sizeof(payload))) {
        break;
      }
      }*/
    //delay(1000);
    /*if (radio_read()) {
      payload.print("Received command");

      process_message();
      radio_write();

      payload.print("Send message");
      }*/
    /*radio.startListening();
      delay(10);
      if (radio.available()) {
      //Serial.println("Incoming transmission");
      radio.read(&payload, sizeof(payload));

      //Serial.print("Processing command: ");
      //Serial.println(payload.cmd);

      if (payload.cmd == 'p') {
        payload.cmd = 'P';
      } *//*else if (payload.cmd == 't') {
  sensors.requestTemperatures();
  float temp = sensors.getTempCByIndex(0);

  //Serial.print("Measured temprerature: ");
  //Serial.println(temp);

  payload.cmd = 'T';
  payload.data = (payload.data << 16) + (unsigned long)(temp * 100);
  } *//*else if (payload.cmd == 'l') {
  int brightness = map(payload.data, 0, 100, 0, 255);
  payload.data = (payload.data << 16) + brightness;
  payload.cmd = 'L';

  /*Serial.print("Set brightness to: ");
  Serial.println(brightness);/*

  analogWrite(LIGHT, brightness);
  }

  radio.stopListening();
  for (int i = 0; i < 10; ++i) {
  payload.retry = i;
  if (radio.write(&payload, sizeof(payload))) {
  //Serial.print("Transmission succeed, retries: ");
  //Serial.println(i);
  break;
  }
  }
  }

  //delay(10);
  radio.stopListening();*/
}

