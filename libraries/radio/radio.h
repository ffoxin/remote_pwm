#pragma once


/// ADDRESSES

byte device[][5] = {
    "1avr",
    "2avr"
};

/// PROTOCOL

struct message_t
{
    uint16_t id;
    uint32_t timestamp;
    uint8_t cmd;
    union
    {
        uint32_t dword;
        uint16_t word[2];
    } data;
    uint8_t retry;
};

struct Command
{
    /// Ping
    /// data:
    ///   dword: sender micros()
    static const uint8_t REQ_PING = 'p';
    static const uint8_t RET_PING = 'P';
    /// Light
    /// data:
    ///   word[0]: light id (1..)
    ///   word[1]: brightness (0..100)
    static const uint8_t REQ_LIGHT = 'l';
    static const uint8_t RET_LIGHT = 'L';
    /// Temp
    /// data:
    ///   word[0]: sensor id (1..)
    ///   word[1]: temp value (C * 100)
    static const uint8_t REQ_TEMP = 't';
    static const uint8_t RET_TEMP = 'T';
};


/// SERIAL

void setup_serial()
{
    Serial.begin(115200);
    Serial.setTimeout(50);
}

void _p(char *fmt, ...)
{
    char buf[128]; // resulting string limited to 128 chars
    va_list args;
    va_start (args, fmt);
    vsnprintf(buf, 128, fmt, args);
    va_end (args);

#ifdef DEBUG
    Serial.print("[");
    Serial.print(micros());
    Serial.print("] ");
    Serial.println(buf);
#endif
}


/// RADIO

RF24 radio(CE_PIN, CSN_PIN);

void radio_print()
{
    // Model
    _p("Model: nRF24L01%s", radio.isPVariant() ? "+" : "");

    // Power
    uint8_t power = radio.getPALevel();
    char const *level;
    if (power == RF24_PA_MIN)
    {
        level = "MIN";
    }
    else if (power == RF24_PA_LOW)
    {
        level = "LOW";
    }
    else if (power == RF24_PA_HIGH)
    {
        level = "HIGH";
    }
    else if (power == RF24_PA_MAX)
    {
        level = "MAX";
    }
    else
    {
        level = "ERROR:UNKNOWN";
    }
    _p("Power amplifier: %s", level);
}

message_t message;

void setup_radio()
{
    radio.begin();
    radio.setPayloadSize(sizeof(message));
    radio.setDataRate(RF24_250KBPS);
    radio.setChannel(115);
    //radio.setAutoAck(1);
    radio.setRetries(15, 15);
    radio.stopListening();

    message = {0, 0, 0, 0, 0};
}

bool radio_read()
{
    radio.startListening();
    delay(10);

    bool result = radio.available();
    if (result)
    {
        radio.read(&message, sizeof(message));

        _p("Received message %hu, retries: %hhu", message.id, message.retry);
    }
    radio.stopListening();

    return result;
}

bool radio_write()
{
    static uint16_t id = 0;

    message.id = ++id;
    // send message max 60ms * 10 = 600ms
    for (int i = 0; i < 10; ++i)
    {
        message.retry = i;
        message.timestamp = micros();
        if (radio.write(&message, sizeof(message)))
        {
            _p("Transmission %hu succeed, retries: %hhu", id, message.retry);

            return true;
        }
    }

    _p("Transmission %hu failed, retries: max", id);

    return false;
}

void make_ping_request()
{
    message.cmd = Command::REQ_PING;
    message.data.dword = 0;
}

void make_ping_response()
{
    message.cmd = Command::RET_PING;
    message.data.dword = message.timestamp;
}
