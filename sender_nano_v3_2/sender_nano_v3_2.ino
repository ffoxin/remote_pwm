#define CE_PIN 9
#define CSN_PIN 10
//#define DEBUG 1

#include <RF24.h>

#include "radio.h"

struct telegram_t
{
    uint8_t cmd;
    uint8_t id;
    uint32_t data;
} telegram;

void telegram_response()
{
#if defined(DEBUG)
    _p("Telegram response: cmd=%hhu id=%hhu data=%lu", telegram.cmd, telegram.id, telegram.data);
#else
    Serial.write((char *)&telegram, sizeof(telegram));
#endif
}

void start_serial()
{
    setup_serial();
    _p("Serial initialized");
}

void start_radio()
{
    setup_radio();

    radio.openReadingPipe(1, device[0]);

    _p("Radio initialized");
    radio_print();
}

void setup()
{
    start_serial();
    start_radio();
}

bool handle_ping_request()
{
    // handle incoming transmission
    make_ping_response();
    radio_write();

    // send reverse ping
    make_ping_request();

    return true;
}


bool handle_ping_response()
{
    uint32_t timestamp = (micros() - message.data.dword) / 1000;
    _p("Ping: %dms", timestamp);

    telegram.cmd = message.cmd;
    telegram.id = 1;
    telegram.data = timestamp;

    telegram_response();

    return false;
}

bool handle_temp_response()
{
    _p("Temp: %ul", message.data.dword);

    telegram.cmd = message.cmd;
    telegram.id = 1;
    telegram.data = message.data.dword;

    telegram_response();

    return false;
}

bool handle_light_response()
{
    _p("Light: %ul", message.data.dword);

    telegram.cmd = message.cmd;
    telegram.id = 1;
    telegram.data = message.data.dword;

    telegram_response();

    return false;
}

bool process_message()
{
    _p("Processing command: %hhu", message.cmd);

    if (message.cmd == Command::REQ_PING)
    {
        return handle_ping_request();
    }
    else if (message.cmd == Command::RET_PING)
    {
        return handle_ping_response();
    }
    else if (message.cmd == Command::RET_TEMP)
    {
        return handle_temp_response();
    }
    else if (message.cmd == Command::RET_LIGHT)
    {
        return handle_light_response();
    }

    return false;
}

void loop(void)
{
    if (radio_read())
    {
        if (process_message())
        {
            radio.openWritingPipe(device[1]);
            radio_write();
        }
    }

    if (Serial.available() > 0)
    {
        if (Serial.readBytes((char *)&telegram, sizeof(telegram)))
        {
            message.cmd = telegram.cmd;
            message.data.dword = telegram.data;

            radio.openWritingPipe(device[telegram.id]);
            radio_write();
        }
    }
    else
    {
        delay(50);
    }
    /*if (radio_read()) {
      if (message.cmd == 'P') {
        message.data.dword = micros() - message.data.dword;
      }
      //Serial.write((unsigned char *)&payload, sizeof(payload));
    }*/

    /*if (Serial.available() > 0) {
      // read the incoming command
      memset(r_buf, 0, 3);
      int count = Serial.readBytes(r_buf, 3);
      if (count) {
        memset(s_buf, 0, 6);
        s_buf[0] = 'R'; // received confirmation
        s_buf[1] = r_buf[0]; // send back command
        s_buf[2] = (unsigned byte)count; // number of bytes received
        Serial.write(s_buf, 6);

        payload.cmd = r_buf[0];
        if (payload.cmd == 'p') {
          payload.data = micros();
        } else {
          payload.data = r_buf[2];
        }

        radio.openWritingPipe(device[r_buf[1]]);
        for (int i = 0; i < 10; ++i) {
          payload.retry = i;
          if (radio.write(&payload, sizeof(payload))) {
            memset(s_buf, 0, 6);
            s_buf[0] = 'S'; // sent confirmation
            s_buf[1] = r_buf[0]; // send back command
            s_buf[2] = i; // number of retries
            break;
          }
        }
      }
      } else {
      delay(50);
      }
      //*///delay(50);
}
